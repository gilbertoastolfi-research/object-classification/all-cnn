from cnn import CNN
import os
from sklearn import metrics
import matplotlib.pyplot as plt
from math import sqrt
import numpy as np
import collections
import itertools

def plot_confusion_matrix(y_true, y_pred, clss, method, normalize, file_name):
	title='Matriz de confusão para ' + method 
	cl_dict = {}
	for k, v in clss.items(): cl_dict[v]=k
	od = collections.OrderedDict(sorted(cl_dict.items()))

	classes = []
	for k, v in od.items():
		classes.append(v)
		
	np.set_printoptions(precision=2)

	cm = metrics.confusion_matrix(y_true, y_pred)

	plt.figure()
	plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
	plt.title(title)
	plt.colorbar()
	tick_marks = np.arange(len(classes))
	plt.xticks(tick_marks, classes, rotation=90)
	plt.yticks(tick_marks, classes)

	if normalize:
		cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
	thresh = cm.max() / 2.
	for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
		plt.text(j, i, format(float(cm[i, j]) * 100, '.2f')+'%',
				 horizontalalignment="center", 
				 color="white" if cm[i, j] > thresh else "black")

	plt.tight_layout()
	plt.savefig(file_name)
	plt.close()

def save_reports(y_true, y_pred, class_indices, file_name):
	target_names = []
	for k, v in class_indices.items():
		target_names.append(k)
	text = metrics.classification_report(y_true, y_pred, target_names=target_names)
	f = open(file_name, "w")
	f.write(text)
	f.close()

if __name__ == '__main__':
	#'FT2P' with fine-tuning with retrain
	#'NOTL' without transfer-learning
	#'FT1P' with fine-tuning no retrain
	#'TL' with transfer-learning
	approach = 'TL'
	dropout= 0.5
	width = 224
	height = 224
	cnn_name='ResNet50'
	batch_size=4
	epochs=2 
	learning_rate=0.0003
	optimizer='SGD'
	repositorio = './data/repetition'
	result_dir = './results'
	
	if not os.path.exists(result_dir):
		os.makedirs(result_dir)
	
	y_pred_all, x_true_all = [], []
	class_indices = None
	for rep in os.listdir(repositorio):
		path_rep = repositorio+'/'+rep
		train_dir = path_rep + '/train'
		validation_dir = path_rep + '/validation' # sem validação atribua None a esta variável.
		test_dir = path_rep + '/test'
		path_models_checkpoints = path_rep  + '/checkpoints_'+rep+'.h5'
		cnn = CNN(approach, dropout, width, height, cnn_name, batch_size, epochs, learning_rate, 
			optimizer, train_dir, validation_dir, test_dir, result_dir, path_models_checkpoints)
		cnn.trainModel()
		y_pred, x_true = cnn.testModel()
		y_pred_all.extend(y_pred)
		x_true_all.extend(x_true)
		class_indices = cnn.get_class_indices()
		path_file_name = result_dir  + '/'+cnn_name+'_'+rep
		plot_confusion_matrix(x_true, y_pred, class_indices, cnn_name, True, path_file_name+'.png')
		save_reports(x_true, y_pred, class_indices, path_file_name+'.txt')
	#salva a média
	path_file_average = result_dir + '/'+cnn_name+'_media'
	save_reports(x_true_all, y_pred_all, class_indices, path_file_average+'.txt')
	plot_confusion_matrix(x_true_all, y_pred_all, class_indices, cnn_name, True, path_file_average+'.png')
	