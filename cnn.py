from __future__ import print_function
import os
import tensorflow as tf
import itertools
import matplotlib.pyplot as plt
import numpy as np
from random import shuffle
from tensorflow.keras.applications import ResNet50, Xception, VGG16, VGG19, ResNet50, InceptionV3, MobileNet, InceptionResNetV2 , DenseNet201, NASNetLarge, NASNetMobile,  MobileNetV2 , ResNet101, ResNet152
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.optimizers import  SGD, Adam, Adagrad
from tensorflow.keras.models import Model, load_model
from tensorflow.keras import backend
from tensorflow.keras.backend import clear_session
from tensorflow.keras.layers import Dropout, Flatten, Dense, GlobalAveragePooling2D
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
import pandas as pd
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # Suppress warnings

'''
device_name = tf.test.gpu_device_name()
if device_name != '/device:GPU:0':
	raise SystemError('\n\nThis error most likely means that this notebook is not configured to use a GPU.  Change this in Notebook Settings via the command palette (cmd/ctrl-shift-P) or the Edit menu.\n\n')
print('Found GPU at: {}'.format(device_name))

'''
device_name = tf.test.gpu_device_name()
if device_name != '/device:GPU:0':
	print('GPU device not found')
print('Found GPU at: {}'.format(device_name))

architectures = {"MobileNet":MobileNet,"Xception":Xception, "VGG16":VGG16, "VGG19":VGG19, "ResNet50":ResNet50,
	"InceptionV3":InceptionV3,"InceptionResNetV2":InceptionResNetV2, "DenseNet201":DenseNet201, 
	"NASNetLarge": NASNetLarge, "NASNetMobile":NASNetMobile,"MobileNetV2":MobileNetV2, "ResNet152":ResNet152, "ResNet101":ResNet101}

optimizers = {'SGD':SGD, 'Adam':Adam , 'Adagrad':Adagrad}
class CNN:
	def __init__(self, approach, dropout, width, height, cnn_name, batch_size, epochs, 
		learning_rate, optimizer, train_dir, validation_dir, test_dir, result_dir, path_models_checkpoints):

		self.approach = self.define_approach(approach)
		self.dropout = dropout

		self.train_dir = train_dir
		self.validation_dir = validation_dir
		self.test_dir = test_dir
		self.path_models_checkpoints = path_models_checkpoints

		self.width = width
		self.height = height
		self.shape = (self.width, self.height, 3)
		self.architecture = cnn_name

		self.batch_size = batch_size
		self.weights = 'imagenet'
		self.epochs = epochs
		self.learning_rate = learning_rate
		self.optimizer = optimizer
		self.model = None

		self.checkpoint, self.early_stop = self.training_callbacks(1, 20)
				
		self.history_validation = None
		self.history_test = None
		self.history_train = None

		self.horizontal_flip = None #True
		self.fill_mode = 'nearest'
		self.zoom_range = 0.3
		self.width_shift_range = 0.3
		self.height_shift_range = 0.3
		self.rotation_range = 30
		self.file_result = self.get_file_names_log()
		self.class_indices = None
		self.result_dir = result_dir	
		
	def get_class_indices(self):
		return self.class_indices

	def get_file_result(self):
		return self.file_result

	def get_checkpoint(self):
		return self.checkpoint

	def get_early_stop(self):
		return self.early_stop

	def get_history_validation(self):
		return self.history_validation
		
	def set_history_validation(self, history_validation):
		self.history_validation = history_validation


	def get_history_test(self):
		return self.history_test
		
	def set_history_test(self, history_test):
		self.history_test = history_test

	def get_history_train(self):
		return self.history_train
		
	def set_history_train(self, history_train):
		self.history_train = history_train

	def get_learning_rate(self):
		return self.learning_rate
		
	def set_learning_rate(self, learning_rate):
		self.learning_rate = learning_rate

	def get_path_models_checkpoints(self):
		return self.path_models_checkpoints
		
	def set_path_models_checkpoints(self, path_models_checkpoints):
		self.path_models_checkpoints = path_models_checkpoints

	def define_approach(self, name):
		ftr = 0
		if name == 'TL': # with transfer-learning
			ftr = 0
		elif name == 'FT2P': # with fine-tuning with retrain
			ftr = 1
		elif name == 'NOTL': # without transfer-learning
			ftr = 2
		elif name == 'FT1P': # with fine-tuning no retrain
			ftr = 3
		return ftr

	def get_approach(self):
		return self.approach
		
	def set_approach(self, approach):
		self.approach = approach

	def get_epochs(self):
		return self.epochs
		
	def set_epochs(self, epochs):
		self.epochs = epochs

	def get_weights(self):
		return self.architecture
		
	def set_weights(self, weights):
		self.weights = weights

	def get_architecture(self):
		return self.architecture
		
	def set_architecture(self, architecture):
		self.architecture = architecture

	def get_batch_size(self):
		return self.batch_size
		
	def set_batch_size(self, batch_size):
		self.batch_size = batch_size

	def get_train_dir(self):
		return self.train_dir
		
	def set_train_dir(self, train_dir):
		self.train_dir = train_dir

	def get_validation_dir(self):
		return self.validation_dir
		
	def set_validation_dir(self, validation_dir):
		self.validation_dir = validation_dir

	def get_test_dir(self):
		return self.test_dir
		
	def set_test_dir(self, test_dir):
		self.test_dir = test_dir

	def get_model(self):
		return self.model
		
	def set_model(self, model):
		self.model = model

	def get_num_classes(self):
		num_classes = 0
		try:
			num_classes = self.get_train_generator().num_class
		except:
			num_classes = self.get_train_generator().num_classes
		return num_classes

	def get_train_generator(self):

		train_datagen = None
		if self.horizontal_flip != None:
			train_datagen = ImageDataGenerator(horizontal_flip=self.horizontal_flip, fill_mode=self.fill_mode, 
				zoom_range=self.zoom_range, width_shift_range=self.width_shift_range, 
				height_shift_range=self.height_shift_range, rotation_range=self.rotation_range)
		else:
			print('Sem aumento de dados')
			train_datagen = ImageDataGenerator()

		train_generator = train_datagen.flow_from_directory(self.get_train_dir(), target_size=(self.height, self.width), 
			batch_size=self.get_batch_size(), shuffle=True, class_mode="categorical")

		self.class_indices = (train_generator.class_indices)
		return train_generator

	def get_validation_generator(self):


		if self.get_validation_dir() == None:
			return None

		validation_datagen = ImageDataGenerator()
		#validation_datagen = ImageDataGenerator()
		validation_generator = validation_datagen.flow_from_directory(self.get_validation_dir(), target_size=(self.height, 
			self.width), batch_size=self.get_batch_size(), shuffle=True, class_mode="categorical")
		return validation_generator

	def get_test_generator(self):
		
		test_datagen = ImageDataGenerator()
		#test_datagen = ImageDataGenerator()
		test_generator = test_datagen.flow_from_directory(self.get_test_dir(), batch_size=1, shuffle=False, 
			target_size=(self.height, self.width), class_mode="categorical")
		return test_generator

	'''
	def gpu_model(self, model):
		try:
			model = multi_gpu_model(model, gpus=1)#, cpu_merge=1) #
		except:
			pass
		return model
	'''
	def get_optimizer(self, learning_rate):
		#return optimizers.Adagrad(lr=learning_rate, epsilon=1e-08, decay=0.01)
		return optimizers[self.optimizer](lr=learning_rate)

	#Initializing the model with random weights and train the model from scratch.
	def training_model_from_scratch(self, architecture):
		num_classes = self.get_num_classes()
		base_model = architecture(weights=None,  # train the model from scratch.
			include_top=False, # Do not include the ImageNet classifier at the top.
			input_shape=self.shape)
		
		#Create a new model on top of the output of one (or several) layers from the base model.
		new_custom_layers = base_model.output
		new_custom_layers = Flatten()(new_custom_layers)
		new_custom_layers = Dense(1024, activation="relu")(new_custom_layers)
		new_custom_layers = Dropout(self.dropout)(new_custom_layers)
		new_custom_layers = Dense(1024, activation="relu")(new_custom_layers)
		predictions = Dense(num_classes, activation="softmax")(new_custom_layers)

		model_final = Model(inputs=base_model.input, outputs=predictions)
		#model_final = self.gpu_model(model_final)
		#Train the model on new data.
		model_final.compile(loss="categorical_crossentropy", optimizer=self.get_optimizer(self.get_learning_rate()), metrics=["acc"])
		self.training(model_final)
		self.make_training_graphics()
	
	'''
	This leads us to how a typical transfer learning workflow can be implemented in Keras:

	Instantiate a base model and load pre-trained weights into it.
	Freeze all layers in the base model by setting trainable = False.
	Create a new model on top of the output of one (or several) layers from the base model.
	Train your new model on your new dataset.
	'''
	#Transfer-learning. Fonte: https://keras.io/guides/transfer_learning/
	def training_model_transfer_learning(self, architecture):
		num_classes = self.get_num_classes()
		base_model = architecture(weights='imagenet',  # Load weights pre-trained on ImageNet.
			include_top=False, # Do not include the ImageNet classifier at the top.
			input_shape=self.shape)
		base_model.trainable = False # Freeze all layers in the base model by setting trainable = False.
		#Create a new model on top of the output of one (or several) layers from the base model.
		
		new_custom_layers = base_model.output
		new_custom_layers = Flatten()(new_custom_layers)
		new_custom_layers = Dense(1024, activation="relu")(new_custom_layers)
		new_custom_layers = Dropout(self.dropout)(new_custom_layers)
		new_custom_layers = Dense(1024, activation="relu")(new_custom_layers)
		predictions = Dense(num_classes, activation="softmax")(new_custom_layers)

		model_final = Model(inputs=base_model.input, outputs=predictions)
		#model_final = self.gpu_model(model_final)
		#Train the model on new data.
		model_final.compile(loss="categorical_crossentropy", optimizer=self.get_optimizer(self.get_learning_rate()), metrics=["acc"])
		
		self.training(model_final)
		self.make_training_graphics()

	#Transfer-learning and fine-tuning one step
	def training_model_fine_tuning_one_step(self, architecture):
		num_classes = self.get_num_classes()
		base_model = architecture(weights='imagenet',  # Load weights pre-trained on ImageNet.
			include_top=False, # Do not include the ImageNet classifier at the top.
			input_shape=self.shape)
		base_model.trainable = True # Unfreeze all layers in the base model to update imagenet weights
		#Create a new model on top of the output of one (or several) layers from the base model.
		
		new_custom_layers = base_model.output
		new_custom_layers = Flatten()(new_custom_layers)
		new_custom_layers = Dense(1024, activation="relu")(new_custom_layers)
		new_custom_layers = Dropout(self.dropout)(new_custom_layers)
		new_custom_layers = Dense(1024, activation="relu")(new_custom_layers)
		predictions = Dense(num_classes, activation="softmax")(new_custom_layers)

		model_final = Model(inputs=base_model.input, outputs=predictions)
		#model_final = self.gpu_model(model_final)
		#Train the model on new data.
		model_final.compile(loss="categorical_crossentropy", optimizer=self.get_optimizer(self.get_learning_rate()), metrics=["acc"])
		
		self.training(model_final)
		self.make_training_graphics()

	#Transfer-learning and fine-tuning.
	def training_model_fine_tuning_two_step(self, architecture):
		num_classes = self.get_num_classes()
		base_model = architecture(weights='imagenet',  # Load weights pre-trained on ImageNet.
			include_top=False, # Do not include the ImageNet classifier at the top.
			input_shape=self.shape)

		base_model.trainable = False # Freeze all layers in the base model by setting trainable = False.
		
		inputs = tf.keras.Input(shape=self.shape)
		# We make sure that the base_model is running in inference mode here,
		# by passing `training=False`. This is important for fine-tuning
		x = base_model(inputs, training=False) 

		#Create a new model on top of the output of one (or several) layers from the base model.
		#new_custom_layers = base_model.output
		new_custom_layers = Flatten()(x)
		new_custom_layers = Dense(1024, activation="relu")(new_custom_layers)
		new_custom_layers = Dropout(self.dropout)(new_custom_layers)
		new_custom_layers = Dense(1024, activation="relu")(new_custom_layers)
		predictions = Dense(num_classes, activation="softmax")(new_custom_layers)

		model_final = Model(inputs=inputs, outputs=predictions)
		#model_final = self.gpu_model(model_final)
		#Train the model on new data on freeze backbone
		model_final.compile(loss="categorical_crossentropy", optimizer=self.get_optimizer(self.get_learning_rate()), metrics=["acc"])
		self.training(model_final)
		model_final = self.get_model()
		# Unfreeze the base_model. Note that it keeps running in inference mode
		# since we passed `training=False` when calling it. This means that
		# the batchnorm layers will not update their batch statistics.
		# This prevents the batchnorm layers from undoing all the training
		# we've done so far.
		base_model.trainable = True
		learning_rate = self.get_learning_rate() / 10 # according to keras documentation "It's also critical to use a very low learning rate at this stage"
		model_final.compile(loss="categorical_crossentropy", optimizer=self.get_optimizer(learning_rate), metrics=["acc"])
		self.training(model_final)
		self.make_training_graphics()

	
	def training_callbacks(self, checkpoints, early_stopping_patience):
		checkpoint = None
		early_stop = None
		if checkpoints == 1: 
			checkpoint = ModelCheckpoint(self.get_path_models_checkpoints(), monitor='acc',
									 verbose=1, save_best_only=True, save_weights_only=False, mode='auto', period=1)
		if early_stopping_patience > 0:
			early_stop = EarlyStopping(monitor='loss', patience=early_stopping_patience)

		return checkpoint, early_stop

	def get_file_names_log(self):
		file_name = ''
		if self.get_approach() == 0: # with transfer-learning
			file_name = '_transfer_learning'
		elif self.get_approach() == 1: # with fine-tuning with retrain
			file_name = '_fine_tuning_two_step'
		elif self.get_approach() == 2: # without transfer-learning
			file_name = '_without_transfer_learning'
		elif self.get_approach() == 3: # with fine-tuning no retrain
			file_name = '_fine_tuning_one_step'
		else:
			print('No logs')
			pass

		file_name = self.get_architecture() + file_name 
		
		txt = os.path.basename(self.get_path_models_checkpoints())
		x = txt.split('_')
		#print(txt)
		x = x[1].split('.')[0]
		file_name = file_name + '_' + x
		file_name = file_name + '_batch_size_'+ str(self.get_batch_size())
		return file_name

	def make_training_graphics(self):
		#file_name = self.get_file_names_log()
		file_name = self.file_result
		history = self.get_history_train()

		# Convert to pandas and save HISTORY as csv
		hist_df = pd.DataFrame(history.history)
		hist_csv_file = self.result_dir + '/'+file_name + '_HISTORY.csv'
		with open(hist_csv_file, mode='w') as f:
			hist_df.to_csv(f)

		if self.get_validation_generator() != None:
			acc = history.history['acc']
			val_acc = history.history['val_acc']
			loss = history.history['loss']
			val_loss = history.history['val_loss']

			print("Stopped Epoch = %s" % self.get_early_stop().stopped_epoch)
			used_epochs = self.get_early_stop().stopped_epoch
			if self.get_early_stop().stopped_epoch == 0:
				used_epochs = self.get_epochs()
			else:
				used_epochs = self.get_early_stop().stopped_epoch + 1

			epochs_range = range(used_epochs)
			plt.figure(figsize=(16, 8))			
			plt.subplot(1, 2, 1)
			plt.plot(epochs_range, acc, label='Training Accuracy')
			plt.plot(epochs_range, val_acc, label='Validation Accuracy')
			plt.legend(loc='lower right')
			plt.title('Training and Validation Accuracy')
			plt.subplot(1, 2, 2)
			plt.plot(epochs_range, loss, label='Training Loss')
			plt.plot(epochs_range, val_loss, label='Validation Loss')
			plt.legend(loc='upper right')
			plt.title('Training and Validation Loss')
			plt.savefig(self.result_dir + '/'+file_name + '_HISTORY.png')



	def training(self, model):
		#model_final.summary()
		callbacks = []
		if self.get_checkpoint() != None:
			callbacks.append(self.get_checkpoint())
		if self.get_early_stop() != None:
			callbacks.append(self.get_early_stop())

		history = None
		if self.get_validation_generator() != None:
			history = model.fit(self.get_train_generator(), 
				steps_per_epoch=self.get_train_generator().samples // self.get_batch_size(), 
				epochs=self.get_epochs(), validation_data=self.get_validation_generator(), 
				validation_steps=self.get_validation_generator().samples // self.get_batch_size(), 
				callbacks=callbacks)			
		else:
			history = model.fit(self.get_train_generator(), 
				steps_per_epoch=self.get_train_generator().samples // self.get_batch_size(), 
				epochs=self.get_epochs(), callbacks=callbacks)
		
		self.set_history_train(history)
		model_final = load_model(self.get_path_models_checkpoints())
		self.set_model(model_final)

	def trainModel(self):

		if self.get_approach() == 0: # with transfer-learning
			self.training_model_transfer_learning(architecture = architectures[self.get_architecture()])
		elif self.get_approach() == 1: # with fine-tuning two step
			self.training_model_fine_tuning_two_step(architecture = architectures[self.get_architecture()])
		elif self.get_approach() == 2: # without transfer-learning
			self.training_model_from_scratch(architecture = architectures[self.get_architecture()])
		elif self.get_approach() == 3: # with fine-tuning one step
			self.training_model_fine_tuning_one_step(architecture = architectures[self.get_architecture()])
		else:
			print('No approach')
	
	def testModel(self):

		test_features = self.get_model().predict_generator(self.get_test_generator(), self.get_test_generator().samples, verbose=1)
		y_pred = np.argmax(test_features, axis=1)
		y_true = self.get_test_generator().classes
		clear_session()
		self.set_model(None)
		return y_pred, y_true